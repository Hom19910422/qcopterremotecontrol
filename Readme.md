﻿QCopter Remote Control
========
* File Name   : Readme.md
* Author      : Hom19910422
* Version     : v1.0
* Date        : 2013/08/01
* Description : QCopterRemoteControl Information
* Wiki        : https://github.com/Hom19910422/QCopterRemoteControl/wiki

遙控板　RemoteControl
========
* 控制器　STM32F407VG 100Pin 168MHz DSP FPU
* 螢幕　5吋螢幕800*480，使用RA8875，FSMC操作
* 儲存紀錄　SD卡，使用SDIO操作
* 感測器　IMU 6DOF (MPU-6050)
* 無線傳輸　nRF24L01P + PA + LNA
* 乙太網路　預計使用W5100，SPI操作
* 1個USB接口　連接電腦、外擴裝置
* 1個UART接口　外擴裝置